docker_start:
	sudo docker-compose up --build

docker_stop:
	docker-compose down

docker_exec_db:
	docker exec -it todoapp_db_1 sh

docker_exec_front:
	docker exec -it todoapp_frontend_1 sh

docker_exec_back:
	docker exec -it todoapp_backend_1 sh