import { Column, Entity, PrimaryGeneratedColumn, BaseEntity } from "typeorm";

@Entity()
export class Todo extends BaseEntity {
  @PrimaryGeneratedColumn()

  id: {
    type: Number,
    primary: true,
    generated: true,
  };

  @Column({ length: 150 })
  title: string;

  @Column()
  description: string;
}