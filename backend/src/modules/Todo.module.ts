import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TodosController } from "src/controllers/Todo.controller";
import { Todo } from "src/entities/Todo.entity";
import { TodoService } from "src/services/Todo.service";

@Module({ 
  imports: [TypeOrmModule.forFeature([Todo])],
  providers: [TodoService],
  controllers: [TodosController],
})

export class TodosModule {}