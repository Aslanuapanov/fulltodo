import { Controller, Body, Get, Delete, Param, Patch, Post } from "@nestjs/common";
import { ParseIntPipe } from "@nestjs/common/pipes/parse-int.pipe";
import { TodoService } from "src/services/Todo.service";
import { Todo } from '../entities/Todo.entity';

@Controller('todos')
export class TodosController {
  constructor(private todoService: TodoService) {}

  @Get()
  getTodos() {
    return this.todoService.getTodos();
  }

  @Get(':id')
  getTodo(@Param('id', ParseIntPipe) id) {
    return this.todoService.findOne(id);
  }

  @Post() create(@Body() todo: Todo) {
    return this.todoService.createTodos(todo);
  }

  @Patch(':id')
  async editTodo(@Body() todo: Todo, @Param('id') id: number): Promise<Todo> {
    const todoEdited = await this.todoService.editTodos(todo, id);
    return todoEdited;
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id) {
    return await this.todoService.remove(id);
  }
}
