import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Todo } from "../entities/Todo.entity";

@Injectable()
export class TodoService {
  constructor(@InjectRepository(Todo) private todoRepository: Repository<Todo>,) {}

  getTodos(): Promise<Todo[]> {
    return this.todoRepository.find();
  }

  findOne(id?: any): Promise<Todo> {
    return this.todoRepository.findOne({ where: { id } });
  }

  createTodos(todo: Todo) {
    return this.todoRepository.save(todo);
  }

  remove(id: string) {
    return this.todoRepository.delete(id);
  }

  async editTodos(todo: Todo, id?: any ): Promise<Todo> {
    const editTodos = await this.todoRepository.findOne({ where: { id } });

    if (!editTodos) {
      throw new NotFoundException('Todo is not found');
    }

    editTodos.description = todo.description;
    editTodos.title = todo.title;

    await editTodos.save();
    return editTodos;
  }
}