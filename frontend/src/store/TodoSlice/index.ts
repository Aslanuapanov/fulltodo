import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getTodos, addTodo, removeTodo, editTodo } from '../../service/TodoService';

interface addType {
  todoText: string,
  setTodoText: any,
  setTodo: any,
}

interface editType {
  todo: Todo,
  setOpenPopup: any,
  setNewText: any,
  setEditId: any,
}

interface removeType {
  id: number,
  setTodo: any,
}

interface newTodoType {
  newText: string,
  editId: number,
  setTodo: any,
  setOpenPopup: any,
}

interface Todo {
  id: number;
  title: string;
  description: string;
}

export const TodoSlice = createSlice({
  name: 'todo',
  initialState: {

  },
  reducers: {
    add: (state, action: PayloadAction<addType>) => {
      if (action.payload.todoText) {
        let data = {
          title: action.payload.todoText,
          description: action.payload.todoText
        }
        addTodo(data, action.payload.setTodoText, action.payload.setTodo);
      }
    },
    edit: (state, action: PayloadAction<editType>) => {
      action.payload.setOpenPopup(true);
      action.payload.setNewText(action.payload.todo.title);
      action.payload.setEditId(action.payload.todo.id);
    },
    remove: (state, action: PayloadAction<removeType>) => {
      return removeTodo(action.payload.id, action.payload.setTodo);
    },
    newTodo: (state, action: PayloadAction<newTodoType>) => {
      if (action.payload.newText) {
        let data: Todo = {
          id: action.payload.editId,
          title: action.payload.newText,
          description: action.payload.newText
        }
    
        editTodo(data, action.payload.setTodo);
        action.payload.setOpenPopup(false);
      }
    },
    getTodo: async (state, action: PayloadAction<any>) => {
      await getTodos(action.payload.setTodo);
    }
  },
})

export const { add, remove, edit, newTodo, getTodo } = TodoSlice.actions;

export default TodoSlice.reducer;