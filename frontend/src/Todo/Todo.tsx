import React, { useState, useEffect } from 'react';
import { AsInput, AsButton } from '@aslanuapanov/as-kit';
import { useDispatch } from 'react-redux';
import { add, remove, edit, newTodo, getTodo } from '../store/TodoSlice/index';
import TodoItem from '../TodoItem/TodoItem';
import './Todo.scss';

interface Todo {
  id: number;
  title: string;
  description: string;
}

function Todo() {
  const [todo, setTodo] = useState<[]>([]);
  const [todoText, setTodoText] = useState<string>('');
  const [openPopup, setOpenPopup] = useState<boolean>(false);
  const [newText, setNewText] = useState<string>('');
  const [editId, setEditId] = useState<number>(0);
  const dispatch = useDispatch();

  const inputTodo = (e: React.FormEvent<HTMLInputElement>) => {
    let value = (e.target as HTMLInputElement).value;
    setTodoText(value);
  }

  const newInputTodo = (e: React.FormEvent<HTMLInputElement>) => {
    let value = (e.target as HTMLInputElement).value;
    setNewText(value);
  }

  useEffect(() => {
    dispatch(getTodo({setTodo}));
  }, []);

  return (
    <div className="todo">
      <div className="todo__title">My Todo App</div>
      <AsInput className="todo__input" onChange={inputTodo} value={todoText} label="Text todo" placeholder="write a text..." />
      <AsButton className="todo__button" onClick={() => dispatch(add({todoText, setTodoText, setTodo}))} primary>Add Todo</AsButton>
      <ul className="list">
        {
          todo.length > 0 ? 
          todo?.map((item: Todo) => <TodoItem key={item.id} todo={item} edit={() => dispatch(edit({todo: item, setOpenPopup, setNewText, setEditId}))} remove={() => dispatch(remove({id: item.id, setTodo}))} primary/> )
          : (
            <div className="todo__empty">
              <p>Not Todos</p>
            </div>
          ) 
        }
      </ul>
      {
        openPopup ? 
          <div className="todo__popup">
            <div className="todo__popup--title">
              Edit Todo
            </div>
            <div className="todo__popup--content">
              <AsInput className="todo__input" onChange={newInputTodo} value={newText} placeholder="write a text..." />
              <AsButton className="todo__button" onClick={() => dispatch(newTodo({newText, editId, setTodo, setOpenPopup}))} primary>Accept</AsButton>
              <AsButton className="todo__button" onClick={() => setOpenPopup(false)}>Exit</AsButton>
            </div>
          </div>
        :
        ''
      }
    </div>
  );
}

export default Todo;
