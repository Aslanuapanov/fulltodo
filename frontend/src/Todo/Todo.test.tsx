import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import axios from 'axios';
import Todo from './Todo';

const mockDispatch = jest.fn();
const mockChildComponent = jest.fn(); 

jest.mock('axios', () => ({
  create: jest.fn()
}));

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('should render sam text passed into title', () => {
  test('should render sam text passed into title', () => {
    render(<Todo />);
    const todoTitle = screen.getByText(/my todo app/i);
    const inputTodo = screen.getByPlaceholderText(/write a text.../i);
    const labelText = screen.getByLabelText(/Text todo/i);
    const buttonTodo = screen.getByRole('button');
  
    expect(todoTitle).toBeInTheDocument();
    expect(labelText).toBeInTheDocument();
    expect(buttonTodo).toBeInTheDocument();
    expect(inputTodo).toBeInTheDocument();
  });
  let response:any;
  beforeEach(() => {
    response = {
      data: [
        {
          id: 1,
        },
        {
          id: 2,
        },
        {
          id: 3,
        }
      ]
    }
  })
  
  test('should render useEffect test', async () => {
    (axios.create as jest.Mock).mockReturnValue({
      get: response
    })
    render(<Todo />);
    const todo = await screen.queryAllByTestId('item-id');
    expect(todo.length).toBeGreaterThanOrEqual(0);
  });

  test('Button click', () => {
    render(<Todo />);
    const btn = screen.getByText(/Add Todo/i);
    fireEvent.click(btn);
  })

  test('Input event', () => {
    render(<Todo />);
    
    const input = screen.getByPlaceholderText(/write a text.../i);
    fireEvent.input(input, {
      target: {
        value: '1233123'
      }
    });
    expect(screen.queryByText(/1233123/i));
  })
});
