import React from 'react';
import { AsButton } from '@aslanuapanov/as-kit';

function TodoItem(props: any) {
  return (
    <li key={props.todo?.id} item-id={props.todo?.id}> 
      <div>{props.todo?.title}</div>
      <div>
        <AsButton className="todo__edit" onClick={() => props.edit()} >edit</AsButton>
        <AsButton className="todo__delete" onClick={() => props.remove()}>delete</AsButton>
      </div>
    </li>
  );
}

export default TodoItem;