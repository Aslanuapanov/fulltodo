import axios from 'axios';

async function getTodos(setTodo: any) {
  await axios.get('http://localhost:3001/todos').then(({ data }) => {
    setTodo(data);
  }).catch(err => {console.log(err)})
}

async function addTodo(data: any, setTodoText: any, setTodo: any) {
  await axios.post('http://localhost:3001/todos', data).then((data) =>{ 
    setTodoText('')
    getTodos(setTodo)
  }).catch(err => {console.log(err)})
}

async function removeTodo(id: number, setTodo: any) {
  await axios.delete('http://localhost:3001/todos/' + id).then(() => {
    getTodos(setTodo)
  }).catch(err => {console.log(err)})
}

async function editTodo(todo: any, setTodo: any) {
  let data = {
    title: todo.title,
    description: todo.description
  }

  await axios.patch('http://localhost:3001/todos/' + todo.id, data).then(() => {
    getTodos(setTodo)
  }).catch(err => {console.log(err)})
}

export {
  getTodos,
  addTodo,
  removeTodo,
  editTodo
}